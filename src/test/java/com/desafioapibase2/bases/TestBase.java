package com.desafioapibase2.bases;

import com.desafioapibase2.GlobalParameters;
import com.desafioapibase2.dbsteps.GerenciarDBSteps;
import com.desafioapibase2.dbsteps.LimparDadosBancoDBSteps;
import com.desafioapibase2.dbsteps.TarefaDBSteps;
import com.desafioapibase2.utils.ExtentReportsUtils;
import org.testng.ITestResult;
import org.testng.annotations.*;

import java.lang.reflect.Method;

public abstract class TestBase {
    @BeforeSuite(alwaysRun = true)
    public void beforSuite() {
        new GlobalParameters();
        ExtentReportsUtils.createReport();
    }

    @BeforeMethod(alwaysRun = true)
    public void beforeTest(Method method) {
        ExtentReportsUtils.addTestWithTags(method.getName(), method.getAnnotation(Test.class).groups());
        //=====================Limpando o lixo do banco de dados=====================//
        LimparDadosBancoDBSteps.limparDadosBD();
        //=======================Preparando a massa de dados=======================//
        GerenciarDBSteps.GerenciarDB();
        TarefaDBSteps.TarefaDB();
    }

    @AfterMethod(alwaysRun = true)
    public void afterTest(ITestResult result) {
        ExtentReportsUtils.addTestResult(result);
    }

    @AfterSuite(alwaysRun = true)
    public void afterSuite() {
        ExtentReportsUtils.addTestEnvironment();
        ExtentReportsUtils.generateReport();
    }
}
