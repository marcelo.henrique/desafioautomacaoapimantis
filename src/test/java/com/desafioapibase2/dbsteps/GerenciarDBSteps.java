package com.desafioapibase2.dbsteps;

import com.desafioapibase2.utils.DBUtils;
import com.desafioapibase2.utils.GeneralUtils;

public class GerenciarDBSteps {

    private static String queriesPath = "src/test/java/com/desafioapibase2/queries/gerenciar/";

    public static void GerenciarDB(){
        CriarProjectDBSteps();
        CriarProjectVersionDBSteps();
        CriarUsuarioAdministradorDBSteps();
        CriarUsuarioDBSteps();
        CriaCampoPersonalizadoDBSteps();
        CriarMarcadorDBSteps();
    }

    public static void CriarProjectDBSteps(){
        String query = GeneralUtils.readFileToAString(queriesPath + "criarProject.sql");
        DBUtils.executeUpdate(query);
    }

    public static void CriarProjectVersionDBSteps(){
        String query = GeneralUtils.readFileToAString(queriesPath + "criarProjectVersion.sql");
        DBUtils.executeUpdate(query);
    }

    public static void CriaCampoPersonalizadoDBSteps(){
        String query = GeneralUtils.readFileToAString(queriesPath + "criarCampoPersonalizado.sql");
        DBUtils.executeUpdate(query);
    }

    public static void CriarCategoryDBSteps(){
        String query = GeneralUtils.readFileToAString(queriesPath + "criarCategory.sql");
        DBUtils.executeUpdate(query);
    }

    public static void CriarMarcadorDBSteps(){
        String query = GeneralUtils.readFileToAString(queriesPath + "criarMarcador.sql");
        DBUtils.executeUpdate(query);
    }

    public static void CriarUsuarioAdministradorDBSteps(){
        String query = GeneralUtils.readFileToAString(queriesPath + "criarUsuarioAdministrador.sql");
        DBUtils.executeUpdate(query);
    }

    public static void CriarUsuarioDBSteps(){
        String query = GeneralUtils.readFileToAString(queriesPath + "criarUsuario.sql");
        DBUtils.executeUpdate(query);
    }
}
