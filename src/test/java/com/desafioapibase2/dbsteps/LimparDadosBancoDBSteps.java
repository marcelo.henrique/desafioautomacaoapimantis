package com.desafioapibase2.dbsteps;

import com.desafioapibase2.utils.DBUtils;
import com.desafioapibase2.utils.GeneralUtils;

public class LimparDadosBancoDBSteps {

    private static String queriesPath = "src/test/java/com/desafioapibase2/queries/limpardadosbanco/";

    public static void limparDadosBD() {
        bugFileTable();
        bugHistoryTable();
        bugMonitorTable();
        bugNoteTable();
        bugNoteTextTable();
        bugRelationshipTable();
        bugRevisionTable();
        bugTable();
        bugTagTable();
        bugTextTable();
        categoryTable();
        customFieldTable();
        emailTable();
        filtersTable();
        projectTable();
        projectVersionTable();
        tagTable();
        userTable();
    }

    private static void bugFileTable() {
        String query =  GeneralUtils.readFileToAString(queriesPath + "bugFileTable.sql");
        DBUtils.executeUpdate(query);
    }

    private static void bugHistoryTable() {
        String query = GeneralUtils.readFileToAString(queriesPath + "bugHistoryTable.sql");
        DBUtils.executeUpdate(query);
    }

    private static void bugMonitorTable() {
        String query = GeneralUtils.readFileToAString(queriesPath + "bugMonitorTable.sql");
        DBUtils.executeUpdate(query);
    }

    private static void bugNoteTable() {
        String query = GeneralUtils.readFileToAString(queriesPath + "bugNoteTable.sql");
        DBUtils.executeUpdate(query);
    }

    private static void bugNoteTextTable() {
        String query = GeneralUtils.readFileToAString(queriesPath + "bugNoteTextTable.sql");
        DBUtils.executeUpdate(query);
    }

    private static void bugRelationshipTable() {
        String query = GeneralUtils.readFileToAString(queriesPath + "bugRelationshipTable.sql");
        DBUtils.executeUpdate(query);
    }

    private static void bugRevisionTable() {
        String query = GeneralUtils.readFileToAString(queriesPath + "bugRevisionTable.sql");
        DBUtils.executeUpdate(query);
    }

    private static void bugTable() {
        String query = GeneralUtils.readFileToAString(queriesPath + "bugTable.sql");
        DBUtils.executeUpdate(query);
    }

    private static void bugTagTable() {
        String query = GeneralUtils.readFileToAString(queriesPath + "bugTagTable.sql");
        DBUtils.executeUpdate(query);
    }

    private static void bugTextTable() {
        String query = GeneralUtils.readFileToAString(queriesPath + "bugTextTable.sql");
        DBUtils.executeUpdate(query);
    }

    private static void categoryTable() {
        String query = GeneralUtils.readFileToAString(queriesPath + "categoryTable.sql");
        DBUtils.executeUpdate(query);
    }

    private static void customFieldTable() {
        String query = GeneralUtils.readFileToAString(queriesPath + "customFieldTable.sql");
        DBUtils.executeUpdate(query);
    }

    private static void emailTable() {
        String query = GeneralUtils.readFileToAString(queriesPath + "emailTable.sql");
        DBUtils.executeUpdate(query);
    }

    private static void filtersTable() {
        String query = GeneralUtils.readFileToAString(queriesPath + "filtersTable.sql");
        DBUtils.executeUpdate(query);
    }

    private static void projectTable() {
        String query = GeneralUtils.readFileToAString(queriesPath + "projectTable.sql");
        DBUtils.executeUpdate(query);
    }

    private static void projectVersionTable() {
        String query = GeneralUtils.readFileToAString(queriesPath + "projectVersionTable.sql");
        DBUtils.executeUpdate(query);
    }

    private static void tagTable() {
        String query = GeneralUtils.readFileToAString(queriesPath + "tagTable.sql");
        DBUtils.executeUpdate(query);
    }

    private static void userTable() {
        String query = GeneralUtils.readFileToAString(queriesPath + "userTable.sql");
        DBUtils.executeUpdate(query);
    }
}


