package com.desafioapibase2.tests;

import com.desafioapibase2.bases.TestBase;
import com.desafioapibase2.requests.projects.DeleteProjectsRequest;
import com.desafioapibase2.requests.projects.GetProjectsRequest;
import com.desafioapibase2.requests.projects.PostProjectVersionRequest;
import com.desafioapibase2.requests.projects.PostProjectsRequest;
import com.desafioapibase2.utils.GeneralUtils;
import com.desafioapibase2.utils.Utils;
import io.restassured.response.Response;
import org.apache.http.HttpStatus;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import static io.restassured.module.jsv.JsonSchemaValidator.matchesJsonSchemaInClasspath;

public class ProjectsTests extends TestBase {

    @Test(groups = {"projects"})
    public void inserirProjetoComSucesso() {
        PostProjectsRequest postProjectsRequest;
        SoftAssert softAssert = new SoftAssert();

        //Parâmetros
        String name = "Projeto Automacao API"+Utils.randomString(2);
        int statusId = 10;
        String statusName = "development";
        String statusLabel = "development";
        String description = "Projeto Teste Automacao API "+ Utils.randomString(10);
        boolean enabled = true;
        String file_path = Utils.randomString(5)+"/"+Utils.randomString(5);
        int view_stateId = 10;
        String view_stateName = "public";
        String view_stateLabel = "public";
        int statusCodeEsperado = HttpStatus.SC_CREATED;

        //Fluxo
        postProjectsRequest = new PostProjectsRequest();
        postProjectsRequest.setJsonBodProjects(name, statusId, statusName, statusLabel, description, enabled, file_path, view_stateId, view_stateName, view_stateLabel);
        Response response = postProjectsRequest.executeRequest();

        //Asserções
        softAssert.assertEquals(response.statusCode(), statusCodeEsperado);
        softAssert.assertEquals(response.jsonPath().get("project.name"), name);
        softAssert.assertEquals(response.jsonPath().get("project.status.id"), Integer.valueOf(statusId));
        softAssert.assertEquals(response.jsonPath().get("project.status.name"), statusName);
        softAssert.assertEquals(response.jsonPath().get("project.status.label"), statusLabel);
        softAssert.assertEquals(response.jsonPath().get("project.description"), description);
        softAssert.assertEquals(response.jsonPath().get("project.enabled"), Boolean.valueOf(enabled));
        softAssert.assertEquals(response.jsonPath().get("project.view_state.id"), Integer.valueOf(view_stateId));
        softAssert.assertEquals(response.jsonPath().get("project.view_state.name"), view_stateName);
        softAssert.assertEquals(response.jsonPath().get("project.view_state.label"), view_stateLabel);
        softAssert.assertAll();
    }

    @Test(groups = {"projects", "contrato"})
    public void testeContratoInserirProjetoComSucesso() {
        PostProjectsRequest postProjectsRequest;
        SoftAssert softAssert = new SoftAssert();

        //Parâmetros
        String name = "Projeto Automacao API"+Utils.randomString(2);
        int statusId = 10;
        String statusName = "development";
        String statusLabel = "development";
        String description = "Projeto Teste Automacao API "+ Utils.randomString(10);
        boolean enabled = true;
        String file_path = Utils.randomString(5)+"/"+Utils.randomString(5);
        int view_stateId = 10;
        String view_stateName = "public";
        String view_stateLabel = "public";
        int statusCodeEsperado = HttpStatus.SC_CREATED;

        //Fluxo
        postProjectsRequest = new PostProjectsRequest();
        postProjectsRequest.setJsonBodProjects(name, statusId, statusName, statusLabel, description, enabled, file_path, view_stateId, view_stateName, view_stateLabel);
        Response response = postProjectsRequest.executeRequest();

        //Asserções
        response.then().assertThat().body(matchesJsonSchemaInClasspath("contratos/PostProjectsComSucessoContrato.json"));
        softAssert.assertEquals(response.statusCode(), statusCodeEsperado);
        softAssert.assertAll();
    }

    @Test(groups = {"projects"})
    public void deletarProjetoComSucesso() {
        DeleteProjectsRequest deleteProjectsRequest;
        SoftAssert softAssert = new SoftAssert();

        //Parâmetros
        int projetoId = 1;
        int statusCodeEsperado = HttpStatus.SC_OK;

        //Fluxo
        deleteProjectsRequest = new DeleteProjectsRequest(projetoId);
        Response response = deleteProjectsRequest.executeRequest();

        //Asserções
        softAssert.assertEquals(response.statusCode(), statusCodeEsperado);
        softAssert.assertAll();
    }

    @Test(groups = {"projects"})
    public void buscarProjetoComSucesso() {
        GetProjectsRequest getProjectsRequest;
        SoftAssert softAssert = new SoftAssert();

        //Parâmetros
        int projetoId = 1;
        int statusCodeEsperado = HttpStatus.SC_OK;

        //Fluxo
        getProjectsRequest = new GetProjectsRequest(projetoId);
        Response response = getProjectsRequest.executeRequest();

        //Asserções
        softAssert.assertEquals(response.statusCode(), statusCodeEsperado);
        softAssert.assertEquals(response.jsonPath().get("projects[0].id"), Integer.valueOf(projetoId));
        softAssert.assertAll();
    }

    @Test(groups = {"projects", "contrato"})
    public void testeContratoBuscarProjetoComSucesso() {
        GetProjectsRequest getProjectsRequest;
        SoftAssert softAssert = new SoftAssert();

        //Parâmetros
        int projetoId = 1;
        int statusCodeEsperado = HttpStatus.SC_OK;

        //Fluxo
        getProjectsRequest = new GetProjectsRequest(projetoId);
        Response response = getProjectsRequest.executeRequest();

        //
        response.then().assertThat().body(matchesJsonSchemaInClasspath("contratos/GetProjectsComSucessoContrato.json"));
        softAssert.assertEquals(response.statusCode(), statusCodeEsperado);
        softAssert.assertAll();
    }

    @Test(groups = {"projects"})
    public void buscarTodosProjetoComSucesso() {
        GetProjectsRequest getProjectsRequest;
        SoftAssert softAssert = new SoftAssert();

        //Parâmetros
        int statusCodeEsperado = HttpStatus.SC_OK;

        //Fluxo
        getProjectsRequest = new GetProjectsRequest();
        Response response = getProjectsRequest.executeRequest();

        //Asserções
        softAssert.assertEquals(response.statusCode(), statusCodeEsperado);
        softAssert.assertAll();
    }

    @Test(groups = {"projects"})
    public void iserirProjectVersionComsucesso() {
        PostProjectVersionRequest postProjectVersionRequest;
        SoftAssert softAssert = new SoftAssert();

        //Parâmetros
        String name = "v1.1.0";
        String description = "Versao descricao_"+Utils.randomString(2);
        boolean released = true;
        boolean obsolete = false;
        String timestamp = GeneralUtils.getNowDate("yyyy-MM-dd");
        int statusCodeEsperado = HttpStatus.SC_NO_CONTENT;

        //Fluxo
        postProjectVersionRequest = new PostProjectVersionRequest(1);
        postProjectVersionRequest.setJsonBodyProjectVersion(name, description, released, obsolete, timestamp);
        Response response = postProjectVersionRequest.executeRequest();

        //sserções
        softAssert.assertEquals(response.statusCode(), statusCodeEsperado);
        softAssert.assertAll();
    }

    @Test(groups = {"projects"})
    public void iserirProjectVersionComVersaoJaCadastradaNoBanco() {
        PostProjectVersionRequest postProjectVersionRequest;
        SoftAssert softAssert = new SoftAssert();

        //Parâmetros
        String name = "v1.0.0";
        String description = "Versao descricao_"+Utils.randomString(2);
        boolean released = true;
        boolean obsolete = false;
        String timestamp = GeneralUtils.getNowDate("yyyy-MM-dd");;
        int statusCodeEsperado = HttpStatus.SC_BAD_REQUEST;

        //Fluxo
        postProjectVersionRequest = new PostProjectVersionRequest(1);
        postProjectVersionRequest.setJsonBodyProjectVersion(name, description, released, obsolete, timestamp);
        Response response = postProjectVersionRequest.executeRequest();

        //sserções
        softAssert.assertEquals(response.statusCode(), statusCodeEsperado);
        softAssert.assertEquals(response.jsonPath().get("message"), "Version '" + name + "' already exists");
        softAssert.assertEquals(response.jsonPath().get("code"), Integer.valueOf(1600));
        softAssert.assertEquals(response.jsonPath().get("localized"), "A version with that name already exists.");
        softAssert.assertAll();
    }

    @Test(groups = {"projects", "contrato"})
    public void testeContratoIserirProjectVersionComVersaoJaCadastradaNoBanco() {
        PostProjectVersionRequest postProjectVersionRequest;
        SoftAssert softAssert = new SoftAssert();

        //Parâmetros
        String name = "v1.0.0";
        String description = "Versao descricao"+Utils.randomString(2);
        boolean released = true;
        boolean obsolete = false;
        String timestamp = GeneralUtils.getNowDate("yyyy-MM-dd");;
        int statusCodeEsperado = HttpStatus.SC_BAD_REQUEST;

        //Fluxo
        postProjectVersionRequest = new PostProjectVersionRequest(1);
        postProjectVersionRequest.setJsonBodyProjectVersion(name, description, released, obsolete, timestamp);
        Response response = postProjectVersionRequest.executeRequest();

        //sserções
        response.then().assertThat().body(matchesJsonSchemaInClasspath("contratos/PostProjectsSemInformarCampoObrigatorioContrato.json"));
        softAssert.assertEquals(response.statusCode(), statusCodeEsperado);
        softAssert.assertAll();
    }
}
