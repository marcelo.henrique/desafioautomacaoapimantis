package com.desafioapibase2.tests;

import com.desafioapibase2.bases.TestBase;
import com.desafioapibase2.dbsteps.*;
import com.desafioapibase2.requests.issues.*;
import com.desafioapibase2.utils.Utils;
import io.restassured.response.Response;
import org.apache.http.HttpStatus;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import java.lang.reflect.Method;

import static io.restassured.module.jsv.JsonSchemaValidator.matchesJsonSchemaInClasspath;

public class IssuesTests extends TestBase {

    @Test(groups = {"issues"})
    public void inserirIssuesComSucesso() {
        PostIssuesRequest postIssuesRequest;
        SoftAssert softAssert = new SoftAssert();

        //Parâmetros
        String summary = "DesafioAutomacaoAPI "+Utils.randomString(2);
        String description = "Descrição DesafioAutomacaoAPI "+Utils.randomString(3);
        String additionalInformation = "Informação DesafioAutomacaoAPI "+Utils.randomString(10);
        int projectId = 1;
        String projectName = "Projeto Mantis";
        int categoryId = 1;
        String categoryName = "General";
        String handlerName = "vboctor";
        int viewStateId = 10;
        String viewStateName = "public";
        String priority = "normal";
        String severity = "trivial";
        String reproducibilityName = "always";
        boolean sticky = false;
        int customFieldsFieldId = 1;
        String customFieldsFieldName = "Campo Personalizado";
        String customFieldsValeu = "";
        String tagsName = "mantishub";
        int statusCodeEsperado = HttpStatus.SC_CREATED;

        //Fluxo
        postIssuesRequest = new PostIssuesRequest();
        postIssuesRequest.setJsonBodyIssues(summary, description, additionalInformation, projectId, projectName,
                categoryId, categoryName, handlerName, viewStateId, viewStateName, priority, severity, reproducibilityName,
                sticky, customFieldsFieldId, customFieldsFieldName, customFieldsValeu, tagsName);
        Response response = postIssuesRequest.executeRequest();

        //Asserções
        softAssert.assertEquals(response.statusCode(), statusCodeEsperado);
        softAssert.assertEquals(response.jsonPath().get("issue.summary"), summary);
        softAssert.assertEquals(response.jsonPath().get("issue.description"), description);
        softAssert.assertEquals(response.jsonPath().get("issue.project.name"), projectName);
        softAssert.assertAll();
    }

    @Test(groups = {"issues"})
    public void inserirIssuesMinimoComSucesso() {
        PostIssuesRequest postIssuesRequest;
        SoftAssert softAssert = new SoftAssert();

        //Parâmetros
        String summary = "DesafioAutomacaoAPI "+Utils.randomString(2);
        String description = "Descrição Desafio AutomacaoAPI "+Utils.randomString(3);
        String categoryName = "General";
        String projectName = "Projeto Mantis";
        int statusCodeEsperado = HttpStatus.SC_CREATED;

        //Fluxo
        postIssuesRequest = new PostIssuesRequest();
        postIssuesRequest.setJsonBodyIssues(summary, description, categoryName, projectName);
        Response response = postIssuesRequest.executeRequest();

        //Asserções
        softAssert.assertEquals(response.statusCode(), statusCodeEsperado);
        softAssert.assertEquals(response.jsonPath().get("issue.summary"), summary);
        softAssert.assertEquals(response.jsonPath().get("issue.description"), description);
        softAssert.assertEquals(response.jsonPath().get("issue.project.name"), projectName);
        softAssert.assertAll();
    }

    @Test(groups = {"issues", "contrato"})
    public void testeContratoInserirIssuesMinimoComSucesso() {
        PostIssuesRequest postIssuesRequest;
        SoftAssert softAssert = new SoftAssert();

        //Parâmetros
        String summary = "DesafioAutomacaoAPI "+Utils.randomString(2);
        String description = "Descrição Desafio AutomacaoAPI "+Utils.randomString(3);
        String categoryName = "General";
        String projectName = "Projeto Mantis";
        int statusCodeEsperado = HttpStatus.SC_CREATED;

        //Fluxo
        postIssuesRequest = new PostIssuesRequest();
        postIssuesRequest.setJsonBodyIssues(summary, description, categoryName, projectName);
        Response response = postIssuesRequest.executeRequest();

        //Asserções
        response.then().assertThat().body(matchesJsonSchemaInClasspath("contratos/PostIssuesMinimoComSucessoContrato.json"));
        softAssert.assertEquals(response.statusCode(), statusCodeEsperado);
        softAssert.assertAll();
    }

    @Test(groups = {"issues"})
    public void inserirIssuesMinimoSemInformarSummary() {
        PostIssuesRequest postIssuesRequest;
        SoftAssert softAssert = new SoftAssert();

        //Parâmetros
        String summary = "";
        String description = "Descrição Desafio AutomacaoAPI "+Utils.randomString(3);
        String categoryName = "General";
        String projectName = "Projeto Mantis";
        int statusCodeEsperado = HttpStatus.SC_BAD_REQUEST;

        //Fluxo
        postIssuesRequest = new PostIssuesRequest();
        postIssuesRequest.setJsonBodyIssues(summary, description, categoryName, projectName);
        Response response = postIssuesRequest.executeRequest();

        //Asserções
        softAssert.assertEquals(response.statusCode(), statusCodeEsperado);
        softAssert.assertEquals(response.jsonPath().get("message"), "Summary not specified");
        softAssert.assertEquals(response.jsonPath().get("code"), Integer.valueOf(11));
        softAssert.assertAll();
    }

    @Test(groups = {"issues", "contrato"})
    public void testeContratoInserirIssuesMinimoSemInformarSummary() {
        PostIssuesRequest postIssuesRequest;
        SoftAssert softAssert = new SoftAssert();

        //Parâmetros
        String summary = "";
        String description = "Descrição Desafio AutomacaoAPI "+Utils.randomString(3);
        String categoryName = "General";
        String projectName = "Projeto Mantis";
        int statusCodeEsperado = HttpStatus.SC_BAD_REQUEST;

        //Fluxo
        postIssuesRequest = new PostIssuesRequest();
        postIssuesRequest.setJsonBodyIssues(summary, description, categoryName, projectName);
        Response response = postIssuesRequest.executeRequest();

        //Asserções
        response.then().assertThat().body(matchesJsonSchemaInClasspath("contratos/PostIssueMinimoSemInformarCampoObrigatorioContrato.json"));
        softAssert.assertEquals(response.statusCode(), statusCodeEsperado);
        softAssert.assertAll();
    }

    @Test(groups = {"issues"})
    public void inserirIssuesMinimoSemInformarDescription() {
        PostIssuesRequest postIssuesRequest;
        SoftAssert softAssert = new SoftAssert();

        //Parâmetros
        String summary = "DesafioAutomacaoAPI "+Utils.randomString(2);
        String description = "";
        String categoryName = "General";
        String projectName = "Projeto Mantis";
        int statusCodeEsperado = HttpStatus.SC_BAD_REQUEST;

        //Fluxo
        postIssuesRequest = new PostIssuesRequest();
        postIssuesRequest.setJsonBodyIssues(summary, description, categoryName, projectName);
        Response response = postIssuesRequest.executeRequest();

        //Asserções
        softAssert.assertEquals(response.statusCode(), statusCodeEsperado);
        softAssert.assertEquals(response.jsonPath().get("message"), "Description not specified");
        softAssert.assertEquals(response.jsonPath().get("code"), Integer.valueOf(11));
        softAssert.assertAll();
    }

    @Test(groups = {"issues", "contrato"})
    public void testeContratoInserirIssuesMinimoSemInformarDescription() {
        PostIssuesRequest postIssuesRequest;
        SoftAssert softAssert = new SoftAssert();

        //Parâmetros
        String summary = "DesafioAutomacaoAPI "+Utils.randomString(2);
        String description = "";
        String categoryName = "General";
        String projectName = "Projeto Mantis";
        int statusCodeEsperado = HttpStatus.SC_BAD_REQUEST;

        //Fluxo
        postIssuesRequest = new PostIssuesRequest();
        postIssuesRequest.setJsonBodyIssues(summary, description, categoryName, projectName);
        Response response = postIssuesRequest.executeRequest();

        //Asserções
        response.then().assertThat().body(matchesJsonSchemaInClasspath("contratos/PostIssueMinimoSemInformarCampoObrigatorioContrato.json"));
        softAssert.assertEquals(response.statusCode(), statusCodeEsperado);
        softAssert.assertAll();
    }

    @Test(groups = {"issues"})
    public void inserirIssuesMinimoSemInformarProjectName() {
        PostIssuesRequest postIssuesRequest;
        SoftAssert softAssert = new SoftAssert();

        //Parâmetros
        String summary = "DesafioAutomacaoAPI "+Utils.randomString(2);
        String description = "Descrição Desafio AutomacaoAPI "+Utils.randomString(3);
        String categoryName = "General";
        String projectName = "";
        int statusCodeEsperado = HttpStatus.SC_BAD_REQUEST;

        //Fluxo
        postIssuesRequest = new PostIssuesRequest();
        postIssuesRequest.setJsonBodyIssues(summary, description, categoryName, projectName);
        Response response = postIssuesRequest.executeRequest();

        //Asserções
        softAssert.assertEquals(response.statusCode(), statusCodeEsperado);
        softAssert.assertEquals(response.jsonPath().get("message"), "Project not specified");
        softAssert.assertEquals(response.jsonPath().get("code"), Integer.valueOf(11));
        softAssert.assertAll();
    }

    @Test(groups = {"issues", "contrato"})
    public void testeContratoInserirIssuesMinimoSemInformarProjectName() {
        PostIssuesRequest postIssuesRequest;
        SoftAssert softAssert = new SoftAssert();

        //Parâmetros
        String summary = "DesafioAutomacaoAPI "+Utils.randomString(2);
        String description = "Descrição Desafio AutomacaoAPI "+Utils.randomString(3);
        String categoryName = "General";
        String projectName = "";
        int statusCodeEsperado = HttpStatus.SC_BAD_REQUEST;

        //Fluxo
        postIssuesRequest = new PostIssuesRequest();
        postIssuesRequest.setJsonBodyIssues(summary, description, categoryName, projectName);
        Response response = postIssuesRequest.executeRequest();

        //Asserções
        response.then().assertThat().body(matchesJsonSchemaInClasspath("contratos/PostIssueMinimoSemInformarCampoObrigatorioContrato.json"));
        softAssert.assertEquals(response.statusCode(), statusCodeEsperado);
        softAssert.assertAll();
    }

    @Test(groups = {"issues"})
    public void deletarIssuesComSucesso() {
        DeleteIssuesRequest deleteIssuesRequest;
        SoftAssert softAssert =new SoftAssert();

        //Parâmetros
        int issuesId = 1;
        int statusCodeEsperado = HttpStatus.SC_NO_CONTENT;

        //Fluxo
        inserirIssuesMinimoComSucesso();
        deleteIssuesRequest = new DeleteIssuesRequest(issuesId);
        Response response = deleteIssuesRequest.executeRequest();

        //Asserções
        softAssert.assertEquals(response.statusCode(), statusCodeEsperado);
        softAssert.assertAll();
    }

    @Test(groups = {"issues"})
    public void deletarIssuesQueNaoExiste() {
        DeleteIssuesRequest deleteIssuesRequest;
        SoftAssert softAssert = new SoftAssert();

        //Parâmetros
        int issuesId = 3;
        int statusCodeEsperado = HttpStatus.SC_NOT_FOUND;

        //Fluxo
        deleteIssuesRequest = new DeleteIssuesRequest(issuesId);
        Response response = deleteIssuesRequest.executeRequest();

        //Asserções
        softAssert.assertEquals(response.statusCode(), statusCodeEsperado);
        softAssert.assertEquals(response.jsonPath().get("message"), "Issue #"+issuesId+" not found");
        softAssert.assertEquals(response.jsonPath().get("code"), Integer.valueOf(1100));
        softAssert.assertEquals(response.jsonPath().get("localized"), "Issue "+issuesId+" not found.");
        softAssert.assertAll();
    }

    @Test(groups = {"issues", "contrato"})
    public void testeContratoDeletarIssuesQueNaoExiste() {
        DeleteIssuesRequest deleteIssuesRequest;
        SoftAssert softAssert = new SoftAssert();

        //Parâmetros
        int issuesId = 3;
        int statusCodeEsperado = HttpStatus.SC_NOT_FOUND;

        //Fluxo
        inserirIssuesMinimoComSucesso();
        deleteIssuesRequest = new DeleteIssuesRequest(issuesId);
        Response response = deleteIssuesRequest.executeRequest();

        //Asserções
        response.then().assertThat().body(matchesJsonSchemaInClasspath("contratos/PostIssueMinimoSemInformarCampoObrigatorioContrato.json"));
        softAssert.assertEquals(response.statusCode(), statusCodeEsperado);
        softAssert.assertAll();
    }

    @Test(groups = {"issues"})
    public void buscarIssuesComSucesso() {
        GetIssuesRequest getIssuesRequest;
        SoftAssert softAssert = new SoftAssert();

        //Parâmetros
        int issuesId = 1;
        int statusCodeEsperado = HttpStatus.SC_OK;

        //Fluxo
        inserirIssuesMinimoComSucesso();
        getIssuesRequest = new GetIssuesRequest(issuesId);
        Response response = getIssuesRequest.executeRequest();

        //Asserções
        softAssert.assertEquals(response.statusCode(), statusCodeEsperado);
        softAssert.assertAll();
    }

    @Test(groups = {"issues"})
    public void buscarIssuesNaoExiste() {
        GetIssuesRequest getIssuesRequest;
        SoftAssert softAssert = new SoftAssert();

        //Parâmetros
        int issuesId = 3;
        int statusCodeEsperado = HttpStatus.SC_NOT_FOUND;

        //Fluxo
        getIssuesRequest = new GetIssuesRequest(issuesId);
        Response response = getIssuesRequest.executeRequest();

        //Asserções
        softAssert.assertEquals(response.statusCode(), statusCodeEsperado);
        softAssert.assertEquals(response.jsonPath().get("message"), "Issue #"+issuesId+" not found");
        softAssert.assertEquals(response.jsonPath().get("code"), Integer.valueOf(1100));
        softAssert.assertEquals(response.jsonPath().get("localized"), "Issue "+issuesId+" not found.");
        softAssert.assertAll();
    }

    @Test(groups = {"issues", "contrato"})
    public void testeContratoBuscarIssuesNaoExiste() {
        GetIssuesRequest getIssuesRequest;
        SoftAssert softAssert = new SoftAssert();

        //Parâmetros
        int issuesId = 3;
        int statusCodeEsperado = HttpStatus.SC_NOT_FOUND;

        //Fluxo
        getIssuesRequest = new GetIssuesRequest(issuesId);
        Response response = getIssuesRequest.executeRequest();

        //Asserções
        response.then().assertThat().body(matchesJsonSchemaInClasspath("contratos/PostIssueMinimoSemInformarCampoObrigatorioContrato.json"));
        softAssert.assertEquals(response.statusCode(), statusCodeEsperado);
        softAssert.assertAll();
    }

    @Test(groups = {"issues"})
    public void inserirMonitorIssueComSucesso() {
        PostMonitorIssuesRequest postMonitorIssuesRequest;
        SoftAssert softAssert = new SoftAssert();

        //Parâmetros
        int issuesId = 1;
        int statusCodeEsperado = HttpStatus.SC_CREATED;

        //Fluxo
        inserirIssuesMinimoComSucesso();
        postMonitorIssuesRequest = new PostMonitorIssuesRequest(issuesId);
        Response response = postMonitorIssuesRequest.executeRequest();

        //Asserções
        softAssert.assertEquals(response.statusCode(), statusCodeEsperado);
        softAssert.assertAll();
    }

    @Test(groups = {"issues", "contrato"})
    public void testeContratoInserirMonitorIssueComSucesso() {
        PostMonitorIssuesRequest postMonitorIssuesRequest;
        SoftAssert softAssert = new SoftAssert();

        //Parâmetros
        int issuesId = 1;
        int statusCodeEsperado = HttpStatus.SC_CREATED;

        //Fluxo
        inserirIssuesMinimoComSucesso();
        postMonitorIssuesRequest = new PostMonitorIssuesRequest(issuesId);
        Response response = postMonitorIssuesRequest.executeRequest();

        //Asserções
        response.then().assertThat().body(matchesJsonSchemaInClasspath("contratos/PostMonitorComSucessoContrato.json"));
        softAssert.assertEquals(response.statusCode(), statusCodeEsperado);
        softAssert.assertAll();
    }

    @Test(groups = {"issues"})
    public void inserirMonitorEspecificoIssueComSucesso() {
        PostMonitorIssuesRequest postMonitorIssuesRequest;
        SoftAssert softAssert = new SoftAssert();

        //Parâmetros
        int issuesId = 1;
        String usersName = "nomeusuario";
        String userNameOrRealName = "User Automacao";
        int statusCodeEsperado = HttpStatus.SC_CREATED;

        //Fluxo
        inserirIssuesMinimoComSucesso();
        postMonitorIssuesRequest = new PostMonitorIssuesRequest(issuesId);
        postMonitorIssuesRequest.setJsonBodyMonitorIssues(usersName, userNameOrRealName);
        Response response = postMonitorIssuesRequest.executeRequest();

        //Asserções
        softAssert.assertEquals(response.statusCode(), statusCodeEsperado);
        softAssert.assertAll();
    }

    @Test(groups = {"issues", "contrato"})
    public void testeContratoInserirMonitorEspecificoIssueComSucesso() {
        PostMonitorIssuesRequest postMonitorIssuesRequest;
        SoftAssert softAssert = new SoftAssert();

        //Parâmetros
        int issuesId = 1;
        String usersName = "nomeusuario";
        String userNameOrRealName = "User Automacao";
        int statusCodeEsperado = HttpStatus.SC_CREATED;

        //Fluxo
        inserirIssuesMinimoComSucesso();
        postMonitorIssuesRequest = new PostMonitorIssuesRequest(issuesId);
        postMonitorIssuesRequest.setJsonBodyMonitorIssues(usersName, userNameOrRealName);
        Response response = postMonitorIssuesRequest.executeRequest();

        //Asserções
        response.then().assertThat().body(matchesJsonSchemaInClasspath("contratos/PostMonitorEspecificoComSucessoContrato.json"));
        softAssert.assertEquals(response.statusCode(), statusCodeEsperado);
        softAssert.assertAll();
    }

    @Test(groups = {"issues"})
    public void inserirTagIssueComSucesso() {
        PostTagIssueRequest postTagIssueRequest;
        SoftAssert softAssert = new SoftAssert();

        //Parâmetros9
        int issuesId = 1;
        int tagId = 1;
        String tagName = "Tag automacao 1";
        int statusCodeEsperado = HttpStatus.SC_CREATED;

        //Fluxo
        inserirIssuesMinimoComSucesso();
        postTagIssueRequest = new PostTagIssueRequest(issuesId);
        postTagIssueRequest.setJsonBodyTagIssue(tagId, tagName);
        Response response = postTagIssueRequest.executeRequest();

        //Asserções
        softAssert.assertEquals(response.statusCode(), statusCodeEsperado);
        softAssert.assertAll();
    }

    @Test(groups = {"issues", "contrato"})
    public void testeContratoInserirTagIssueComSucesso() {
        PostTagIssueRequest postTagIssueRequest;
        SoftAssert softAssert = new SoftAssert();

        //Parâmetros
        int issuesId = 1;
        int tagId = 1;
        String tagName = "Tag automacao 1";
        int statusCodeEsperado = HttpStatus.SC_CREATED;

        //Fluxo
        inserirIssuesMinimoComSucesso();
        postTagIssueRequest = new PostTagIssueRequest(issuesId);
        postTagIssueRequest.setJsonBodyTagIssue(tagId, tagName);
        Response response = postTagIssueRequest.executeRequest();

        //Asserções
        response.then().assertThat().body(matchesJsonSchemaInClasspath("contratos/PostTagIssueComSucessoContrato.json"));
        softAssert.assertEquals(response.statusCode(), statusCodeEsperado);
        softAssert.assertAll();
    }

    @Test(groups = {"issues"})
    public void inserirRelatiobshipIssueComSucesso() {
        PostRelatiobshipIssueRequest postRelatiobshipIssueRequest;
        SoftAssert softAssert = new SoftAssert();

        //Parâmetros
        int issuesId = 1;
        int issueRelatioId = 2;
        String issueTypeName = "related-to";
        int statusCodeEsperado = HttpStatus.SC_CREATED;

        //Fluxo
        inserirIssuesMinimoComSucesso();
        postRelatiobshipIssueRequest = new PostRelatiobshipIssueRequest(issuesId);
        postRelatiobshipIssueRequest.setJsonBodyRelatiobshipIssues(issueRelatioId, issueTypeName);
        Response response = postRelatiobshipIssueRequest.executeRequest();

        //Asserções
        softAssert.assertEquals(response.statusCode(), statusCodeEsperado);
        softAssert.assertAll();
    }

    @Test(groups = {"issues", "contrato"})
    public void testeContratoInserirRelatiobshipIssueComSucesso() {
        PostRelatiobshipIssueRequest postRelatiobshipIssueRequest;
        SoftAssert softAssert = new SoftAssert();

        //Parâmetros
        int issuesId = 1;
        int issueRelatioId = 2;
        String issueTypeName = "related-to";
        int statusCodeEsperado = HttpStatus.SC_CREATED;

        //Fluxo
        inserirIssuesMinimoComSucesso();
        postRelatiobshipIssueRequest = new PostRelatiobshipIssueRequest(issuesId);
        postRelatiobshipIssueRequest.setJsonBodyRelatiobshipIssues(issueRelatioId, issueTypeName);
        Response response = postRelatiobshipIssueRequest.executeRequest();

        //Asserções
        response.then().assertThat().body(matchesJsonSchemaInClasspath("contratos/PostRelatiobshipIssueComSucessoContrato.json"));
        softAssert.assertEquals(response.statusCode(), statusCodeEsperado);
        softAssert.assertAll();
    }
}
