package com.desafioapibase2.utils;

import static com.desafioapibase2.utils.GeneralUtils.formatJson;
import static org.apache.logging.log4j.util.PropertiesUtil.getSystemProperties;

import java.util.Map;
import java.util.Properties;

import org.testng.ITestResult;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.reporter.ExtentSparkReporter;
import com.desafioapibase2.GlobalParameters;
import com.desafioapibase2.enums.AuthenticationType;

import io.restassured.http.Header;
import io.restassured.response.Response;

public class ExtentReportsUtils
{
    public static ExtentReports EXTENT_REPORT = null;
    public static ExtentTest TEST;
    public static ExtentSparkReporter SPARK_REPORTER = null;
    static String reportName = GlobalParameters.REPORT_NAME + "_" + GeneralUtils.getNowDate("yyyy-MM-dd_HH-mm-ss");
    static String reportsPath = GlobalParameters.REPORT_PATH;
    static String fileName = reportName+".html";
    static String fullReportFilePath = reportsPath + "/"+ reportName +"/" + fileName;
    private static String testNameMemory = null;

    public static void createReport(){
        if (EXTENT_REPORT == null)
        {
            SPARK_REPORTER = new ExtentSparkReporter(fullReportFilePath);
            EXTENT_REPORT = new ExtentReports();
            SPARK_REPORTER.config().setEncoding("UTF-8");
            EXTENT_REPORT.attachReporter(SPARK_REPORTER);
        }
    }

    public static void addTest(String testName, String testCategory){
        TEST = EXTENT_REPORT.createTest(testName).assignCategory(testCategory.replace("Tests",""));
    }

    public static void addRestTestInfo(int methodLevel, String text){
        TEST.log(Status.PASS, GeneralUtils.getMethodNameByLevel(methodLevel) + " || " + text);
    }

    public static void addRestTestInfo(String url, String requestService, String method, Map<String, String> headers, Map<String, String> cookies, Map<String, String> queryParameters,Map<String, String> multiParts, Object jsonBody, AuthenticationType authenticationType, String authenticatorUser, String authenticatorPassword, Response response) {
        ExtentTest node = TEST.createNode("&#128640; Request - " + method + " - " + requestService);

        String allHeaders = "";
        String allCookies = "";
        String allParameters = "";
        String allResponseHeaders = "";
        String allMultiParts = "";

        for (Map.Entry<String, String> queryParameter : queryParameters.entrySet()) {
            allParameters = allParameters + "\n" + "<i>" + queryParameter.getKey() + "</i>" + " = " + queryParameter.getValue();
        }

        for (Map.Entry<String, String> multiPart : multiParts.entrySet()) {
            allMultiParts = allMultiParts + "\n" + "<i>" + multiPart.getKey() + "</i>" + " = " + multiPart.getValue();
        }

        for (Map.Entry<String, String> header : headers.entrySet()) {
            allHeaders = allHeaders + "\n" + "<i>" + header.getKey() + "</i>" + " = " + header.getValue();
        }

        for (Map.Entry<String, String> cookie : cookies.entrySet()) {
            allCookies = allCookies + "\n" + "<i>" + cookie.getKey() + "</i>" + " = " + cookie.getValue();
        }

        for (Header responseHeader : response.getHeaders().asList()) {
            allResponseHeaders = allResponseHeaders + "\n" + responseHeader.getName() + ": " + responseHeader.getValue();
        }

        node.log(Status.INFO, "<pre>" + "<b>URL: </b>" + url + "</pre>");
        node.log(Status.INFO, "<pre>" + "<b>REQUEST: </b>" + requestService + "</pre>");
        node.log(Status.INFO, "<pre>" + "<b>METHOD: </b>" + method + "</pre>");

        if (!authenticationType.equals(AuthenticationType.NONE)) {
            node.log(Status.INFO, "<pre>" + "<b>AUTHENTICATOR TYPE: " + authenticationType.toString() + "</b>" + "\n" + "<b>USER: </b>" + authenticatorUser + "\n" + "<b>PASSWORD: </b>" + authenticatorPassword + "</pre>");
        }

        if (!allHeaders.equals("")) {
            node.log(Status.INFO, "<pre>" + "<b>HEADERS: </b>" + "\n" + allHeaders + "</pre>");
        }

        if (!allCookies.equals("")) {
            node.log(Status.INFO, "<pre>" + "<b>COOKIES: </b>" + "\n" + allCookies + "</pre>");
        }

        if (!allParameters.equals("")) {
            node.log(Status.INFO, "<pre>" + "<b>PARAMETERS: </b>" + "\n" + allParameters + "</pre>");
        }

        if (!allMultiParts.equals("")) {
            node.log(Status.INFO, "<pre>" + "<b>MULTIPARTS: </b>" + "\n" + allMultiParts + "</pre>");
        }

        if (jsonBody != null) {
            if (jsonBody instanceof String) {
                node.log(Status.INFO, "<pre>" + "<b>JSON BODY: </b>" + "\n" + jsonBody.toString() + "</pre>");
            } else {
                node.log(Status.INFO, "<pre>" + "<b>JSON BODY: </b>" + "\n" + GeneralUtils.formatJson(GeneralUtils.convertObjectToJsonString(jsonBody)) + "</pre>");
            }
        }

        node.log(Status.INFO, "<pre>" + "<b>STATUS CODE: </b>" + response.statusCode() + "</pre>");
        node.log(Status.INFO, "<pre>" + "<b>RESPONSE HEADERS: </b>" + "\n" + allResponseHeaders + "</pre>");
        try {
            node.log(Status.INFO, "<pre>" + "<b>PAYLOAD: </b>" + "\n" + GeneralUtils.formatJson(response.body().jsonPath().get().toString()) + "</pre>");
        } catch (io.restassured.path.json.exception.JsonPathException e) {

        }
    }


    public static void addSoapTestInfo(String url, Map<String, String> headers, Map<String, String> cookies, String xmlBody, AuthenticationType authenticationType, String authenticatorUser, String authenticatorPassword, Response response){
        String allHeaders = "";
        String allCookies = "";
        String allResponseHeaders = "";

        for(Map.Entry<String, String> header : headers.entrySet()){
            allHeaders = allHeaders + "\n" + "<i>" + header.getKey() + "</i>" + " = " + header.getValue();
        }

        for(Map.Entry<String, String> cookie : cookies.entrySet()){
            allCookies = allCookies + "\n" + "<i>" + cookie.getKey() + "</i>" + " = " + cookie.getValue();
        }

        for(Header responseHeader : response.getHeaders().asList()){
            allResponseHeaders = allResponseHeaders + "\n" + responseHeader.getName() + ": " + responseHeader.getValue();
        }

        TEST.log(Status.INFO, "<pre>" + "<b>URL: </b>" + url + "</pre>");
        TEST.log(Status.INFO, "<pre>" + "<b>REQUEST: </b><textarea>" + xmlBody + "</textarea>");

        if (!allHeaders.equals("")){
            TEST.log(Status.INFO, "<pre>" + "<b>HEADERS: </b>" + "\n" + allHeaders + "</pre>");
        }

        if (!allCookies.equals("")){
            TEST.log(Status.INFO, "<pre>" + "<b>COOKIES: </b>" + "\n" + allCookies + "</pre>");
        }

        if (!authenticationType.equals(AuthenticationType.NONE)){
            TEST.log(Status.INFO, "<pre>" + "<b>AUTHENTICATOR TYPE: "+authenticationType.toString()+"</b>" + "\n" + "<b>USER: </b>"+ authenticatorUser + "\n" + "<b>PASSWORD: </b>" + authenticatorPassword + "</pre>");
        }

        TEST.log(Status.INFO, "<pre>" + "<b>STATUS CODE: </b>" + response.statusCode() + "</pre>");
        TEST.log(Status.INFO, "<pre>" + "<b>RESPONSE HEADERS: </b>" + "\n" + allResponseHeaders + "</pre>");
        TEST.log(Status.INFO, "<b>PAYLOAD: </b> <textarea>" + "\n" + formatJson(response.body().jsonPath().get().toString()) + "</textarea>");
    }

    public static void addTestResult(ITestResult result){
        switch (result.getStatus())
        {
            case ITestResult.FAILURE:
                TEST.log(Status.FAIL, "Test Result: " + Status.FAIL + "<pre>" + "Message: " + result.getThrowable().toString() + "</pre>" + "<pre>" + "Stack Trace: " + GeneralUtils.getAllStackTrace(result) + "</pre>");
                break;
            case ITestResult.SKIP:
                TEST.log(Status.SKIP, "Test Result: " + Status.SKIP + "<pre>" + "Message: " + result.getThrowable().toString() + "</pre>" + "<pre>" + "Stack Trace: " + GeneralUtils.getAllStackTrace(result) + "</pre>");
                break;
            default:
                TEST.log(Status.PASS, "Test Result: " + Status.PASS);
                break;
        }
    }

    public static void generateReport(){
        EXTENT_REPORT.flush();
    }

    public static void addTestWithTags(String testName, String[] testCategory){
        testNameMemory = testName;
        TEST = EXTENT_REPORT.createTest(testName).assignCategory(testCategory[0]);
    }

    public static void addTestEnvironment(){
        Properties systemProperties = getSystemProperties();

        EXTENT_REPORT.setSystemInfo("OS Name:",  systemProperties.getProperty("os.name"));
        EXTENT_REPORT.setSystemInfo("OS Arq:", systemProperties.getProperty("os.arch"));
        EXTENT_REPORT.setSystemInfo("Java Version:", systemProperties.getProperty("java.version"));
        EXTENT_REPORT.setSystemInfo("URL default:", GlobalParameters.URL_DEFAULT);
        EXTENT_REPORT.setSystemInfo("Data Base URL:", GlobalParameters.DB_URL);
    }

    public static void addTestDataBaseStepInfoPass(int methodLevel, String query){
        String methodName = GeneralUtils.getMethodNameByLevel(methodLevel);

        ExtentTest node = TEST.createNode("&#128331; </b>DATA BASE STEP: </b>" + methodName);
        TEST.log(Status.PASS, "<b>DATA BASE STEP: </b>"+ methodName);
        node.log(Status.INFO, "<pre>" + "<b>QUERY: </b>" + query + "</pre>");
    }

    public static void addTestDataBaseStepInfoError(int methodLevel, String query){
        String methodName = GeneralUtils.getMethodNameByLevel(methodLevel);

        ExtentTest node = TEST.createNode("&#128331; </b>DATA BASE STEP: </b>" + methodName);
        TEST.log(Status.FAIL, "<b>DATA BASE STEP: </b>"+ methodName);
        node.log(Status.INFO, "<pre>" + "<b>QUERY: </b>" + query + "</pre>");
    }
}