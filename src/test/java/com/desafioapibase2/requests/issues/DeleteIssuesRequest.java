package com.desafioapibase2.requests.issues;

import com.desafioapibase2.bases.RequestRestBase;
import io.restassured.http.Method;

public class DeleteIssuesRequest extends RequestRestBase {

    public DeleteIssuesRequest(int issuesId){
        requestService = "/issues/"+issuesId;
        method = Method.DELETE;
    }
}
