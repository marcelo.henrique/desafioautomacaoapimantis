package com.desafioapibase2.requests.issues;

import com.desafioapibase2.bases.RequestRestBase;
import io.restassured.http.Method;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class PostTagIssueRequest extends RequestRestBase {

    public PostTagIssueRequest(int issuetId) {
        requestService = "/issues/" + issuetId + "/tags";
        method = Method.POST;
    }

    public void setJsonBodyTagIssue(int tagId, String tagName) {

        Map<String, Object> JSON = new HashMap<String, Object>();
        Map<String, Object> JSONTagsId = new HashMap<String, Object>();
        Map<String, Object> JSONTagsName = new HashMap<String, Object>();

        JSONTagsId.put("id", tagId);
        JSONTagsName.put("name", tagName);
        JSON.put("tags", new Object[]{JSONTagsId, JSONTagsName});

        jsonBody = JSON;
    }
}
