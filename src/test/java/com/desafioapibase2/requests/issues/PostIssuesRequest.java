package com.desafioapibase2.requests.issues;

import com.desafioapibase2.bases.RequestRestBase;
import io.restassured.http.Method;

import java.util.HashMap;
import java.util.Map;

public class PostIssuesRequest extends RequestRestBase {

    public PostIssuesRequest() {
        requestService = "/issues/";
        method = Method.POST;
    }

    public void setJsonBodyIssues(String summary, String description, String additional_information, int projectId, String projectName, int categoryId, String categoryName,
                                  String handlerName, int view_stateId, String view_stateName, String priorityName, String severityName, String reproducibilityName,
                                  boolean sticky, int custom_fieldsFieldId, String custom_fieldsFieldName, String custom_fieldsValue, String tagsName) {

        Map<String, Object> JSON = new HashMap<String, Object>();
        Map<String, Object> Project = new HashMap<String, Object>();
        Map<String, Object> Category = new HashMap<String, Object>();
        Map<String, Object> Handler = new HashMap<String, Object>();
        Map<String, Object> View_state = new HashMap<String, Object>();
        Map<String, Object> Priority = new HashMap<String, Object>();
        Map<String, Object> Severity = new HashMap<String, Object>();
        Map<String, Object> Reproducibility = new HashMap<String, Object>();
        Map<String, Object> Field = new HashMap<String, Object>();
        Map<String, Object> Custom_fields = new HashMap<String, Object>();
        Map<String, Object> Tags = new HashMap<String, Object>();

        JSON.put("summary", summary);
        JSON.put("description", description);
        JSON.put("additional_information", additional_information);
        Project.put("id", projectId);
        Project.put("name", projectName);
        JSON.put("project", Project);
        Category.put("id", categoryId);
        Category.put("name", categoryName);
        JSON.put("category", Category);
        Handler.put("name", handlerName);
        JSON.put("handler", Handler);
        View_state.put("id", view_stateId);
        View_state.put("name", view_stateName);
        JSON.put("view_state", View_state);
        Priority.put("name", priorityName);
        JSON.put("priority", Priority);
        Severity.put("name", severityName);
        JSON.put("severity", Severity);
        Reproducibility.put("name", reproducibilityName);
        JSON.put("reproducibility", Reproducibility);
        JSON.put("sticky", sticky);
        Field.put("id", custom_fieldsFieldId);
        Field.put("name", custom_fieldsFieldName);
        Custom_fields.put("field", Field);
        Custom_fields.put("value", custom_fieldsValue);
        JSON.put("custom_fields",new Object[]{Custom_fields});
        Tags.put("name", tagsName);
        JSON.put("tags", new Object[]{Tags});

        jsonBody = JSON;
    }

    public void setJsonBodyIssues(String summary, String description, String categoryName, String projectName) {

        Map<String, Object> JSON = new HashMap<String, Object>();
        Map<String, Object> Project = new HashMap<String, Object>();
        Map<String, Object> Category = new HashMap<String, Object>();

        JSON.put("summary", summary);
        JSON.put("description", description);
        Category.put("name", categoryName);
        JSON.put("category", Category);
        Project.put("name", projectName);
        JSON.put("project", Project);

        jsonBody = JSON;
    }
}
