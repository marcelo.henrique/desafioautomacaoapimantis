package com.desafioapibase2.requests.filters;

import com.desafioapibase2.bases.RequestRestBase;
import io.restassured.http.Method;

public class DeleteFilterRequest extends RequestRestBase {

    public DeleteFilterRequest(int filtersId){
        requestService = "/filters/"+filtersId;
        method = Method.DELETE;
    }
}
