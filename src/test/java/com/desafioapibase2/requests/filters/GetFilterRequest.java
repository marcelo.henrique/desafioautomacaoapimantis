package com.desafioapibase2.requests.filters;

import com.desafioapibase2.bases.RequestRestBase;
import io.restassured.http.Method;

public class GetFilterRequest extends RequestRestBase {

    public GetFilterRequest(int filtersId){
        requestService = "/filters/"+filtersId;
        method = Method.GET;
    }

    public GetFilterRequest(){
        requestService = "/filters/";
        method = Method.GET;
    }
}
