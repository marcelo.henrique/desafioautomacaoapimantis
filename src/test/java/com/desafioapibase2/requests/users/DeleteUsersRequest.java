package com.desafioapibase2.requests.users;

import com.desafioapibase2.bases.RequestRestBase;
import io.restassured.http.Method;

public class DeleteUsersRequest extends RequestRestBase {

    public DeleteUsersRequest(int userId){
        requestService = "/users/"+userId;
        method = Method.DELETE;
    }
}
