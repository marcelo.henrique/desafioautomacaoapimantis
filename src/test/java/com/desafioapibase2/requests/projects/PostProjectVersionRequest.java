package com.desafioapibase2.requests.projects;

import com.desafioapibase2.bases.RequestRestBase;
import io.restassured.http.Method;

import java.util.HashMap;
import java.util.Map;

public class PostProjectVersionRequest extends RequestRestBase {

    public PostProjectVersionRequest(int idProject) {
        requestService = "/projects/" + idProject + "/versions/";
        method = Method.POST;
    }

    public void setJsonBodyProjectVersion(String name, String description, boolean released, boolean obsolete, String timestamp) {

        Map<String, Object> JSONBody = new HashMap<String, Object>();

        JSONBody.put("name", name);
        JSONBody.put("description", description);
        JSONBody.put("released", released);
        JSONBody.put("obsolete", obsolete);
        JSONBody.put("timestamp", timestamp);

        jsonBody = JSONBody;
    }
}
