package com.desafioapibase2.requests.projects;

import com.desafioapibase2.bases.RequestRestBase;
import io.restassured.http.Method;

public class GetProjectsRequest extends RequestRestBase {

    public GetProjectsRequest(int projectId){
        requestService = "/projects/"+projectId;
        method = Method.GET;
    }

    public GetProjectsRequest(){
        requestService = "/projects/";
        method = Method.GET;
    }
}
