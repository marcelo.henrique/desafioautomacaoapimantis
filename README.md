## Java Restassured ExtentReports

- Arquitetura Projeto
  - Linguagem	- [Java](https://www.java.com/pt-BR// "Java")
  - [Java Kit Development versão 8](https://www.oracle.com/br/java/technologies/javase/javase-jdk8-downloads.html)
  - Gestão de dependências - [Maven](https://maven.apache.org)
  - Orquestrador de testes - [TestNG](https://testng.org/doc/ "TestNG")
  - Relatório de testes automatizados - [ExtentReports 4.0.9](http://www.extentreports.com/docs/versions/4/java/index.html "ExtentReports 4.0.9")

## Setup para executar o projeto

- [Java JDK](https://www.oracle.com/br/java/technologies/javase/javase-jdk8-downloads.html) instalado na máquina e configurado a variávrel de ambiente `JAVA_HOME` apontando para o local de instalaçao do JDK.
- Versão community do [Intelij](https://www.jetbrains.com/pt-br/idea/download/#section=windows) (ou outro IDE para a linguagem Java) instalada no computador.
- Ambiente da API Mantis criado em docker, instalar o [Docker](https://www.docker.com/products/docker-desktop) na máquina.
- Clonar o projeto em sua máquina:
  `git clone https://gitlab.com/marcelo.henrique/desafioautomacaoapimantis.git`
- Executar o comando > `docker-compose.exe up -d` no diretório do arquivo **docker-compose.yml** (Presente no projeto)
- Abrir o projeto com o **IntelliJ IDEA**, construi-lo e acessar a pasta tests que estará todos os testes realizados


****
## Arquitetura


**Arquitetura padrão Base2**

Foi utilizado a arquitetura proposta pela Base2. A arquitetura se encontra na imagem abaixo:
![alt text](https://i.imgur.com/wexOWJF.png)

****

**Relatório de testes**

Após toda execução: sucesso ou falha, é gerado um relatório com cada passo realizado do teste. Está disponível na pasta do projeto "target/reports". Existe a possibilidade de tirar screenshots em cada passo ou somente em caso de falha. Verifique o arquivo globalParameteres.properties e coloque true ou false no parâmetro: get.screenshot.for.each.step.


**Execução dinâmica via prompt de comando**

Caso queira executar todos os testes, deve-se executar o comando

```
mvn clean test -Dsuite=api
```

**Threads**

Os testes estão sendo divididos em duas threads dentro do ambiente de desenvolvimento.

****

## Projeto

src.teste.java.com.desafiobase2.bases

```
Contém as bases de request e de teste, para executar as requests e os testes
RequestRestBase (A base para executar as requests, a base da request está em RestAssuredUtils)
TestBase (Relatórios e a massa de dados são iniciados por essa classe)
```

src.teste.java.com.desafiobase2.dbsteps

`Contém os métodos de execução das queries`

src.teste.java.com.desafiobase2.queries

```
Contém as queries que serão executadas no banco de dados
gerenciar(contém as queries em SQL referente a gerenciar)
limpardadosbanco(contém as queries em SQL referente a limpardadosbanco)
tarefa(contém as queries em SQL referente a tarefa)
```

src.teste.java.com.desafiobase2.request

```
Contém as requests (POST, GET, DELETE) de cada endpoit
filters (Requests referentes ao endpoint de filters)
issues (Requests referentes ao endpoint de issues)
projects (Requests referentes ao endpoint de projects)
users (Requests referentes ao endpoint de users)
```

src.teste.java.com.desafiobase2.tests

```
Classes onde serão executados os testes.
Foram executados 50 casos de testes.
Em UsersTests foi inserido no método inserirUsuarioJaCadastrado() a validação usando REGEX (Expressões Regulares).
IssuesDataDrivenCSVTest e UsersDataDrivenCSVTest são executados os testes utilizando Data-Driven.
```

src.teste.java.com.desafiobase2.utils

```
DBUtils classe de conexão ao Banco de Dados e métodos para executar a query no banco
ExtentReportsUtils classe de parametrização do relatório
GeneralUtils classe contendo alguns métodos para
RestAssuredUtils classe contendo o métodos para executar a request
Utils classe que contém um método para preencher uma string aleatóriamente
```

src.teste.java

```
issuesSucesso.csv aquivo do tipo CSV contendo os parametros para a execução do Data-Driven nos testes de issues
usersFail.csv aquivo do tipo CSV contendo os parametros para a execução do Data-Driven nos testes de users
usersSucesso.csv aquivo do tipo CSV contendo os parametros para a execução do Data-Driven nos testes de users
```

globalParameters.properties

```
Pode ser executado em diferentes ambientes, alterar o campo enviroment

enviroment=hml (Em qual ambiente quer ser executado os testes)
report.name=DesafioAPIBase (Nome do relatório)
report.path=target/reports/ (local onde será salvo o relatótio)
hml.url.default=http://localhost:8989/api/rest (Base url do ambiente a ser testado)
hml.db.url=localhost:3306 (url e porta do banco de dados)
hml.db.name=bugtracker (Nome do banco)
hml.db.user=root (usuário de acesso ao banco)
hml.db.password=root (senha de acesso ao banco)
hml.url.token=xxx 
hml.authenticator.user=xxx
hml.authenticator.password=xxx
hml.token=0Wxw6B63Bi6Ac3o9A-T9KCQT5GwYdMsl (token de autenticação da API)
```

****

## Configuração pipeline

**Pipeline configurada na AzureDevops**

Execução da pipeline:

![alt text](https://i.imgur.com/Arj3pb9.jpg)

Gráfico gerado após a execução:

![alt text](https://i.imgur.com/u3Y1zGk.jpg)

Relatório de testes gerado:

![alt text](https://i.imgur.com/6GgubRk.jpg)
![alt text](https://i.imgur.com/AJL3PB5.jpg)
![alt text](https://i.imgur.com/68KgDPm.jpg)